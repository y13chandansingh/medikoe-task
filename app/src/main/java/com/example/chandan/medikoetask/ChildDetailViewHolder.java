package com.example.chandan.medikoetask;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by chandan on 22/11/17.
 */

public class ChildDetailViewHolder extends ChildViewHolder {

    private TextView genderName;
    private TextView height;
    private TextView hairColor;
    private TextView mass;

    public ChildDetailViewHolder(View itemView) {
        super(itemView);
        genderName = itemView.findViewById(R.id.gender);
        height = itemView.findViewById(R.id.height);
        hairColor = itemView.findViewById(R.id.hairColor);
        mass = itemView.findViewById(R.id.mass);
    }


    public void setChildDetail(Details childDetail) {
        genderName.setText(childDetail.getGender());
        height.setText(""+childDetail.getHeight());
        hairColor.setText(childDetail.getHair_color());
        mass.setText(""+childDetail.getMass());
    }
}

