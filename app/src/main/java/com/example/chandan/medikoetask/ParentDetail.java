package com.example.chandan.medikoetask;

import android.annotation.SuppressLint;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chandan on 22/11/17.
 */
@SuppressLint("ParcelCreator")
public class ParentDetail  extends ExpandableGroup<ChildDetail> {
    private String name;
    private ArrayList<ChildDetail> childDetails;

    public String getName() {
        return name;
    }

    public ArrayList<ChildDetail> getChildDetails() {
        return childDetails;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChildDetails(ArrayList<ChildDetail> childDetails) {
        this.childDetails = childDetails;
    }

    public ParentDetail(String title, List<ChildDetail> items) {
        super(title, items);
    }
}
