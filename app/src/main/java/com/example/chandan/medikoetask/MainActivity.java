package com.example.chandan.medikoetask;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RecyclerViewAdapter mAdapter;
    private RecyclerView recyclerView;
    private RelativeLayout mRvProgressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void initView() {
        recyclerView = (RecyclerView) findViewById(R.id.rvDetail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRvProgressbar = (RelativeLayout) findViewById(R.id.rlProgressBar);
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder1 = new OkHttpClient.Builder();
        builder1.addInterceptor(httpLoggingInterceptor);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl("https://swapi.co/api/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(builder1.build());
        Retrofit retrofit = builder.build();
        RestClient restClient = retrofit.create(RestClient.class);
        Call<ApiResponse> call = restClient.getStarWarsDetails(1);
        mRvProgressbar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                ArrayList<Details> details = response.body().getResults();
                ArrayList<ParentDetail> parentDetails = new ArrayList<>();
                for (int i =0 ; i< details.size() ; i++) {
                    ArrayList<ChildDetail> childDetail = new ArrayList<>();
                    childDetail.add(details.get(i));
                    ParentDetail parentDetail = new ParentDetail(details.get(i).getName() , childDetail);
                    parentDetails.add(i , parentDetail);
                }
                mRvProgressbar.setVisibility(View.GONE);
                mAdapter = new RecyclerViewAdapter( parentDetails, MainActivity.this );
                recyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<ApiResponse> call, Throwable t) {
                mRvProgressbar.setVisibility(View.GONE);
            }
        });
    }
}
