package com.example.chandan.medikoetask;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ProgressBar;

import java.util.ArrayList;

/**
 * Created by chandan on 22/11/17.
 */

public class ChildDetail implements Parcelable{
    private Details results;

    protected ChildDetail(Parcel in) {
        results = in.readParcelable(Details.class.getClassLoader());
    }

    public static final Creator<ChildDetail> CREATOR = new Creator<ChildDetail>() {
        @Override
        public ChildDetail createFromParcel(Parcel in) {
            return new ChildDetail(in);
        }

        @Override
        public ChildDetail[] newArray(int size) {
            return new ChildDetail[size];
        }
    };

    public Details getResults() {
        return results;
    }

    public void setResults(Details results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable((Parcelable) results, i);
    }
}
