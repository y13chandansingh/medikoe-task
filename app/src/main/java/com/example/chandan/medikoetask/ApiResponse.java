package com.example.chandan.medikoetask;

import java.util.ArrayList;

/**
 * Created by chandan on 22/11/17.
 */

public class ApiResponse {
    private ArrayList<Details> results;

    public ArrayList<Details> getResults() {
        return results;
    }

    public void setResults(ArrayList<Details> results) {
        this.results = results;
    }
}
