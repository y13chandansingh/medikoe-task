package com.example.chandan.medikoetask;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by chandan on 31/10/17.
 */


public interface RestClient {

    @GET("people/")
    Call<ApiResponse> getStarWarsDetails(
            @Query("page") int page
    );
}
