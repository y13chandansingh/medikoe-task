package com.example.chandan.medikoetask;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by chandan on 22/11/17.
 */

public class RecyclerViewAdapter extends ExpandableRecyclerViewAdapter<ParentNameVIewHolder, ChildDetailViewHolder> {

    private MainActivity mActivity;

    public RecyclerViewAdapter(List<? extends ExpandableGroup> groups ,MainActivity mActivity) {
        super(groups);
        this.mActivity = mActivity;
    }

    @Override
    public ParentNameVIewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_item_parent_name, parent, false);
        return new ParentNameVIewHolder(view);
    }

    @Override
    public ChildDetailViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_item_child_detail, parent, false);
        return new ChildDetailViewHolder(view);
    }

    @Override
    public void onBindChildViewHolder(ChildDetailViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        final Details childDetail = (Details) group.getItems().get(0);
       holder.setChildDetail(childDetail);
    }

    @Override
    public void onBindGroupViewHolder(ParentNameVIewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setParentHeaderName(group);
    }
}
